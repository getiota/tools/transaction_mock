# Transaction mocks

A set of scripts to generate transaction data.

## Requirements

- Python > 3.7

## Usage

```bash
python3 generate_transaction.py
```
