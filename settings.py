import datetime
import utils
import random

START_DATE = datetime.datetime(2019, 9, 30)
MAX_TRANSACTIONS = 6
INCOME = round(random.randrange(1800, 2800), 2)
RENT = round(random.randrange(400, 700), 2)

BASE_INCOME = {
    'amount': INCOME,
    'title': 'Thales SA',
    'type': 'sct_in',
    'category': 'income',
    'transfer': {
        'iban': utils.random_iban(),
    },
    'description': 'Ref ZX676JJF9FR',
}

BASE_TOP_UP = {
    'title': 'Alexis Flavier',
    'type': 'sct_in',
    'transfer': {
        'iban': utils.random_iban(),
    },
    'description': 'Remboursement',
}

BASE_RENT = {
    'title': 'Martine Pertuy',
    'amount': -RENT,
    'type': 'sct_out',
    'category': 'housing',
    'type': 'sct_out',
    'transfer': {
        'iban': utils.random_iban(),
    },
    'description': 'Loyer',
}

BASE_FIRST_TRANSFER = {
    'title': 'Sophie Martin',
    'type': 'sct_in',
    'transfer': {
        'iban': utils.random_iban(),
    },
    'description': 'Premier Approvisionnement',
}

BASE_BANK_FEES = {
    'title': 'Abonnement iota',
    'type': 'bank_fee',
    'category': 'bank',
    'description': 'Compte iota - Abonnement mensuel',
}
