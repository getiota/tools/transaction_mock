import random

def random_iban():
    def r(digits):
        return random.randrange(0, 10 ** digits)
    return f'FR{r(2):02d} {r(4):04d} {r(4):04d} {r(4):04d} {r(4):04d} {r(4):04d} {r(3):03d}'


def safe_choice(array):
    if not array:
        return None
    return random.choice(array)

if __name__ == "__main__":
    print(random_iban())