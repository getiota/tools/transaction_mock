#!/usr/bin/env python3

import datetime
import json
import random
import uuid

from pathlib import Path

import settings
import utils
from insights import write_insights_from_transactions

def make_amount(data):
    value = random.uniform(data['min'], data['max'])
    roundTo = data.get('roundTo', 0.01)
    value = round(value / roundTo) * roundTo
    return round(value * data.get('sign', -1), 2)


def make_date(date: datetime.datetime):
    return date.replace(hour=random.randint(8, 20),
                        minute=random.randint(0, 59))


def make_transaction(date, builder):
    selected = random.choices(builder['data'], weights=builder['weights'])[0]
    base = {
        'title': random.choice(selected['names']),
        'amount': make_amount(selected['amount']),
        'date': make_date(date).isoformat(),
        'type': selected.get('type', 'card_payment'),
        'category': selected.get('category'),
        'description': utils.safe_choice(selected.get('descriptions')),
        'location': utils.safe_choice(selected.get('locations')),
    }
    if base['type'].startswith('card'):
        base['card'] = { 'last_digits': '3456' }
    elif base['type'].startswith('sct'):
        base['transfer'] = { 'iban': utils.random_iban() }

    return base


def transactions_for_day(date, builder, amount):
    transactions = []

    if date.day == 1:
        transactions.append({
            **settings.BASE_INCOME,
            'date': make_date(date).isoformat(),
        })
        amount += settings.INCOME
    elif date.day == 3:
        if amount < settings.RENT:
            # Inserting bogus transfer just in case we don't have enough cash
            # to pay rent
            approv = round((settings.RENT - amount + random.randrange(10, 100))/100) * 100
            transactions.append({
                **settings.BASE_TOP_UP,
                'amount': round(approv, 2),
                'date': make_date(date).isoformat(),
            })
            amount += approv

        transactions.append({
            **settings.BASE_RENT,
            'date': make_date(date).isoformat(),
        })
        amount -= settings.RENT
    elif date.day == 16:
        fee = 5.50
        transactions.append({
            **settings.BASE_BANK_FEES,
            'amount': -fee,
            'date': make_date(date).isoformat(),
        })
        amount -= fee

    for _ in range(0, random.randint(0, settings.MAX_TRANSACTIONS)):
        transaction = make_transaction(date, builder)
        new_amount = amount + transaction['amount']
        if new_amount < 0:
            continue
        amount = new_amount
        transactions.append(transaction)

    return amount, sorted(transactions, key=lambda s: s['date'])


if __name__ == "__main__":
    root = Path('.')
    out = root / 'out'
    if not out.exists():
        out.mkdir()

    start_amount = random.randrange(400, 500)
    amount = start_amount
    today = datetime.datetime.now()

    print(f'Income is {settings.INCOME}, Starting at {amount}')

    with open(root / 'build_data.json') as file:
        build_data = json.load(file)

    builder = {
        'data': build_data,
        'weights': [d.get('weight', 1) for d in build_data]
    }

    transactions = [{
        **settings.BASE_FIRST_TRANSFER,
        'amount': start_amount,
        'date': make_date(settings.START_DATE).isoformat(),
    }]
    amounts = [start_amount]

    delta = today - settings.START_DATE
    for i in range(1, delta.days + 1):
        date = settings.START_DATE + datetime.timedelta(days=i)

        amount, ts = transactions_for_day(date, builder, amount)
        amounts.append(round(amount, 2))

        transactions += ts

    print(f'Ended at {amount}')

    # Add Ids
    for t in transactions:
        t['id'] = str(uuid.uuid4())

    # Sanity Checks

    for amt in amounts:
        assert amt >= 0

    transaction_sum = sum((t['amount'] for t in transactions))
    print(transaction_sum,  amounts[-1])
    assert round(transaction_sum - amounts[-1], 2) == 0

    # Payloads

    transactions_json = list(reversed(transactions))

    amounts_json = {
        'amounts': list(reversed(amounts)),
        'first_date': today.date().isoformat(),
    }

    # Outputs
    print(f'Generated {len(transactions)} transactions')
    out_transactions_path = out / 'transactions.json'

    with open(out_transactions_path, 'w') as file:
        json.dump(transactions_json, file, indent=4)

    print(f'File is {out_transactions_path.stat().st_size / 1000}kb')
    print(f'Generated {len(amounts)} amounts')

    out_amt_path = out / 'amounts.json'
    with open(out_amt_path, 'w') as file:
        json.dump(amounts_json, file, indent=4)
    print(f'File is {out_amt_path.stat().st_size / 1000}kb')

    # Make insights

    print('Generating insights')
    write_insights_from_transactions(transactions)
