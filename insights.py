import json

def organize_by_months(transactions):
    by_months = {}
    for t in transactions:
        month = t['date'][:7]
        by_months[month] = by_months.get(month, []) + [t]
    return by_months

def category_sums(transactions):
    by_category = {}
    for t in transactions:
        category = t.get('category', 'other')
        by_category[category] = by_category.get(category, 0) + t['amount']
    return by_category

def data_by_category(transactions, criteria):
    sums = category_sums(filter(criteria, transactions))
    total = sum(sums.values())

    by_category = [
        { 'amount': round(value, 2), 'ratio': round(value / total, 4), 'category': key }
        for (key, value)
        in sums.items()
    ]
    return sorted(by_category, key=lambda k: k['ratio'], reverse=True)

def category_insights(transactions):
    return {
        'spending': data_by_category(transactions, lambda k: k['amount'] < 0),
        'income': data_by_category(transactions, lambda k: k['amount'] >= 0),
    }

def insights_from_transactions(transactions):
    by_months = organize_by_months(transactions)
    return sorted([
        { "date": f'{month}-01T12:00:00', **category_insights(transactions) }
        for month, transactions
        in by_months.items()
    ], key=lambda k: k['date'], reverse=True)

def write_insights_from_transactions(transactions):
    insights = insights_from_transactions(transactions)

    with open('./out/insights.json', 'w') as file:
        json.dump(insights, file, indent=4)

if __name__ == "__main__":
    with open('./out/transactions.json') as file:
        transactions = json.load(file)

    write_insights_from_transactions(transactions)